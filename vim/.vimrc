"
"

" Map <space> as leader key
let mapleader = " "
set ignorecase
colorscheme elflord
set hlsearch
set incsearch
set tabstop=2 " set number of spaces per tab
set shiftwidth=2 " set one indent to be this many characters
set noswapfile
set nobackup
set nostartofline
set timeout timeoutlen=200 ttimeoutlen=100
map <Leader>f :cscope find t <cfile><C-M>

" tag list is not very useful, comment out for now
" autocmd BufRead *.c nnoremap <buffer> :let Tlist_Auto_Open = 1
" let Tlist_Auto_Open = 1
let Tlist_Use_Right_Window = 1
" Open the Tlist Function window
nnoremap <silent> <F8> :TlistToggle<CR>
" CCTreeLoadDB cscope.out
" :set fillchars+=vert:│
map <Leader>w :w<C-M>
map <Leader>v :vsplit<C-M>
map <F2> :res -2<C-M>
map <F3> :res +2<C-M>
map <F4> :wincmd 

" resize window to 80 columns
map <F5> :vertical resize 80<C-M>
map <F6> :let @/ = expand('<cword>')<C-M>

set rtp+=~/bin

" get the cscope db loaded so we can search for terms
" check if there is a cscope.out file in the current directory
" and if there is then load it, otherwise don't. and load it 
" quietly.
if filereadable("cscope.out")
  silent cs a cscope.out
endif
filetype plugin on
set path+=**
set wildmenu
command! MakeTags !ctags -R .
command! MakeCscope !cscope -R .
